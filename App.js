import React from 'react';
import {WebView} from 'react-native-webview';

const App = () => {
  return (
    <WebView
      source={{uri: 'https://hashbanks.vercel.app/#/'}}
      style={{marginTop: 20}}
    />
  );
};

export default App;
