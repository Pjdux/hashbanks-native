import React from 'react';
import {WebView} from 'react-native-webview';

const HashbanksView = () => {
  return (
    <WebView source={{uri: 'https://logrocket.com/'}} style={{marginTop: 20}} />
  );
};

export default HashbanksView;
